import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    footerActive:0,//底部导航
    tabActive:"1",//tabb
    formData:{},//所有的表单数据
    dias:{
      acheet:{ //下拉选择
        show:false,
        columns:[],
        confirm:function(){console.log('confirm')}
      },
      datetime:{
        dateShow:false,
        timeShow:false,
        datetimeShow:false,
        type:'',//时间类型
        confirm:function(){console.log('datetime')}
      },
      studentSelect:{
        show:false,
        selectedNumber:[],//选中的签到数组
        confirm:function(){console.log('studentSelect')}
      },
      anotherSelect:{
        show:false,
        confirm:function(){console.log('anotherSelect')}
      },
      randomSign:{
        data:[],
        show:false,
      }
    }
  },
  mutations: {

  },
  actions: {

  }
})
