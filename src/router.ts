import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/login/login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    }, {
      path: '/login',
      name: 'login',
      component: Login
    }, {
      path: 'what?',
      name: 'what',
      component: () => import('./components/model.vue'),
      children: [
        {
          path: '/home',
          name: "home",
          component: () => import('./views/home/menu.vue'),
          meta:{
            label:"学工宝",
          }
        }, {
          path: '/center',
          component: () => import('./views/center/center.vue'),
          meta: {
            label:"我的",
            urls: {
              list: "users/info",
            }
          }
        },{
          path:"/message",
          redirect:"/message/receive",
          component: () => import('./views/message/message.vue'),
          children:[
            {
              path:"/message/receive",
              component: () => import('./views/message/receive.vue'),
              meta:{
                label:"接收消息",
                urls:{
                  list:"message/receiveMsg"
                }
              }
            },{
              path:"/message/send",
              component: () => import('./views/message/send.vue'),
              meta:{
                label:"发出消息",
                urls:{
                  list:"message/issueMsg"
                }
              }
            }
          ]
        }

      ]
    },

    // 生活服务
    {
      path: '/lifeServe/email',
      name: 'ls_email',
      component: () => import('./views/home/lifeServe/email/email.vue'),
      meta: {
        label:"教师邮箱",
        urls: {
          list: "service/teachermail"
        }
      }
    }, {
      path: '/lifeServe/subway',
      name: 'ls_subway',
      component: () => import('./views/home/lifeServe/subway/subway.vue'),
      meta: {
        label:"火车站路线",
        urls: {
          list: "service/trainstation"
        }
      }
    }, {
      path: '/lifeServe/schoolbus',
      name: 'ls_schoolbus',
      component: () => import('./views/home/lifeServe/schoolbus/schoolbus.vue'),
      meta: {
        label:"校车时刻",
        urls: {
          list: "service/schoolbus"
        }
      }
    }, {
      path: '/lifeServe/calendar',
      name: 'ls_calendar',
      component: () => import('./views/home/lifeServe/calendar/calendar.vue'),
      meta: {
        label:"学期日历",
        urls: {
          list: "service/calendar"
        }
      }
    }, {
      path: '/lifeServe/phone',
      name: 'ls_phone',
      component: () => import('./views/home/lifeServe/phone/phone.vue'),
      meta: {
        label:"校园电话",
        urls: {
          list: "service/schooltel"
        }
      }
    }, {
      path: '/lifeServe/opinions',
      name: 'ls_opinions',
      component: () => import('./views/home/lifeServe/opinions/opinions.vue'),
      meta: {
        label:"意见征集",
        urls: {
          list: "opinion/query",
        }
      }
    }, {
      path: '/lifeServe/opinions/detail/:id',
      name: 'ls_opinions_detail',
      component: () => import('./views/home/lifeServe/opinions/model/detail.vue'),
      meta: {
        label:"意见征集详情",
        urls: {
          detail: "opinion/reply",
          deleteDetail: "opinion/delOpinion",
          editDetail: "opinion/replyInsert"
        }
      }
    }, {
      path: '/lifeServe/support',
      name: 'ls_support',
      component: () => import('./views/home/lifeServe/support/support.vue'),
      meta: {
        label:"资助申请",
        urls: {
          list: "opinion/subsidize",
        }
      }
    },{
      path:'/lifeServe/rating',
      name: 'ls_rating',
      component: () => import('./views/home/lifeServe/rating/rating.vue'),
      meta: {
        label:"评比评选",
        urls: {
          list: "Questionnaire/listVote",
        }
      }
    },{
      path:'/lifeServe/rating/detail/:id',
      component: () => import('./views/home/lifeServe/rating/model/detail.vue'),
      meta:{
        label:"评比评选详情",
        urls:{
          detail:"Questionnaire/resultVote"
        }
      }
    },{
      path:'/lifeServe/rating/add',
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"评比评选添加",
        urls:{
          add:"Questionnaire/addVote"
        }
      }
    },

    // 就业服务
    {
      path:"/jobServe/workstudy",
      component: () => import('./views/home/jobServe/workstudy/workstudy.vue'),
      meta:{
        label:"勤工助学",
        urls:{
          list:"job/qgzxList"
        }
      }
    },{
      path:"/jobServe/workstudy/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"勤工助学添加",
        urls:{
          add:"Job/addPart"
        }
      }
    },{
      path:"/jobServe/recruit",
      component: () => import('./views/home/jobServe/recruit/recruit.vue'),
      meta:{
        label:"招聘信息",
        urls:{
          list:"job/zhaopinList"
        }
      }
    },{
      path:"/jobServe/recruit/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"招聘信息添加",
        urls:{
          add:"Job/addPublishJob"
        }
      }
    },{
      path:"/jobServe/preach",
      redirect:"/jobServe/preach/school",
      component: () => import('./views/home/jobServe/preach/preach.vue'),
      children:[
        {
          path:"/jobServe/preach/school",
          component: () => import('./views/home/jobServe/preach/school.vue'),
          meta:{
            label:"学院发布",
            urls:{
              list:"job/xuanjiangList",
            }
          }
        },{
          path:"/jobServe/preach/others",
          component: () => import('./views/home/jobServe/preach/others.vue'),
          meta:{
            label:"其他来源",
            urls:{
              list:"job/preachList"
            }
          }
        }
      ]
    },{
      path:"/jobServe/preach/school/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"学院发布添加",
        urls:{
          add:"Job/addPublishJob"
        }
      }
    },{
      path:"/jobServe/alumni",
      component: () => import('./views/home/jobServe/alumni/alumni.vue'),
      children:[
        {
          path:"/jobServe/alumni/industry",
          component: () => import('./views/home/jobServe/alumni/industry.vue'),
          meta:{
            label:"行业动态",
            urls:{
              list:"job/qywhList"
            }
          }
        },{
          path:"/jobServe/alumni/culture",
          component: () => import('./views/home/jobServe/alumni/culture.vue'),
          meta:{
            label:"企业文化",
            urls:{
              list:"job/qywhList"
            }
          }
        },{
          path:"/jobServe/alumni/celebrity",
          component: () => import('./views/home/jobServe/alumni/celebrity.vue'),
          meta:{
            label:"校友名录",
            urls:{
              list:"job/alumnuslist"
            }
          }
        }
      ]
    },{
      path:"/jobServe/alumni/industry/detail/:id",
      component: () => import('./views/home/jobServe/alumni/model/detail.vue'),
      meta:{
        label:"行业动态详情",
        urls:{
          detail:"job/qywhDetail"
        }
      }
    },{
      path:"/jobServe/alumni/culture/detail/:id",
      component: () => import('./views/home/jobServe/alumni/model/detail.vue'),
      meta:{
        label:"企业文化详情",
        urls:{
          detail:"job/qywhDetail"
        }
      }
    },

    // 学习服务
    {
      path:"/studyServe/lecture",
      component: () => import('./views/home/studyServe/lecture/lecture.vue'),
      meta:{
        label:"讲座报告",
        urls:{
          list:"chair/query"
        }
      }
    },{
      path:"/studyServe/lecture/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"讲座报告添加",
        urls:{
          add:"chair/add"
        }
      }
    },{
      path:"/studyServe/exam",
      component: () => import('./views/home/studyServe/exam/exam.vue'),
      meta:{
        label:"学习考试",
        urls:{
          list:"keyan/sourceList"
        }
      }
    },{
      path:"/studyServe/exam/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"学习考试添加",
        urls:{
          add:"keyan/sourceAdd"
        }
      }
    },{
      path:"/studyServe/question",
      component:()=>import('./views/home/studyServe/question/question.vue'),
      meta:{
        label:"问卷调研",
        urls:{
          list:"Questionnaire/questionnaire"
        }
      }
    },{
      path:"/studyServe/question/detail/:id",
      component:()=>import('./views/home/studyServe/question/model/detail.vue'),
      meta:{
        label:"问卷调研详情",
        urls:{
          detail:"questionnaire/detailques"
        }
      }
    },{
      path:"/studyServe/research",
      component:()=>import('./views/home/studyServe/research/research.vue'),
      children:[
        {
          path:"/studyServe/research/notice",
          component: () => import('./views/home/studyServe/research/notice.vue'),
          meta:{
            label:"通知",
            urls:{
              list:"keyan/noticeList",
              select:["keyan/category"]
            }
          }
        },{
          path:"/studyServe/research/order",
          component: () => import('./views/home/studyServe/research/order.vue'),
          meta:{
            label:"招募令",
            urls:{
              list:"keyan/zhaomuList",
              select:["keyan/category"]
            }
          }
        },
      ]
    },{
      path:"/studyServe/research/notice/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"通知添加",
        urls:{
          add:"keyan/noticeAdd",
          select:["keyan/category"]
        }
      }
    },{
      path:"/studyServe/research/order/add",
      component: () => import('./views/home/lifeServe/rating/model/add.vue'),
      meta:{
        label:"招募令添加",
        urls:{
          add:"keyan/zhaomuAdd",
          select:["keyan/category"]
        }
      }
    },

    // 统一工作流
      {
        path:"/workflow/teaching",
        component:()=>import('./views/home/workflow/teaching/teaching.vue'),
        meta:{
          label:"教学工作量",
          urls:{
            list:"WorkTime/worktime",
            select:["WorkTime/getYears"]
          }
        }
      },{
        path:"/workflow/teaching/feedback",
        component:()=>import('./views/home/workflow/teaching/feedback.vue'),
        meta:{
          label:"教学工作量反馈",
          urls:{
            list:"WorkTime/myFeedback",
            select:["WorkTime/getYears"]
          }
        }
      },{
        path:"/workflow/invigilate",
        component:()=>import('./views/home/workflow/invigilate/invigilate.vue'),
        meta:{
          label:"监考信息",
          urls:{
            list:"invigilate/invigilateInfo",
            select:["Invigilate/getteacherlist"]
          }
        }
      },{
        path:"/workflow/invigilate/records",
        component:()=>import('./views/home/workflow/invigilate/records.vue'),
        meta:{
          label:"监考信息记录",
          urls:{
            list:"invigilate/weekList",
          }
        }
      },{
        path:"/workflow/student",
        component:()=>import('./views/home/workflow/student/student.vue'),
        meta:{
          label:"学生考核",
          urls:{
            list:"user/assessmentList",
            select:["Invigilate/getteacherlist","WorkTime/getYears","Users/getMyMajors","Users/getMyClass"]
          }
        }
      },{
        path:"/workflow/records",
        component:()=>import('./views/home/workflow/records/records.vue'),
        meta:{
          label:"会议记录",
          urls:{
            list:"user/meetingList"
          }
        }
      },{
        path:"/workflow/records/detail/:id",
        component:()=>import('./views/home/workflow/records/model/detail.vue'),
        meta:{
          label:"会议记录详情",
          urls:{
            detail:"User/meetingDetailed"
          }
        }
      },{
        path:"/workflow/input",
        redirect:"/workflow/input/family",
        component:()=>import("./views/home/workflow/input/input.vue"),
        children:[
          {
            path:"/workflow/input/family",
            component:()=>import("./views/home/workflow/input/family.vue"),
            meta:{
              label:"录入",
              urls:{
                list:"ratify/familylist",
                select:["Users/getMyClass"]
              }
            }
          },{
            path:"/workflow/input/punish",
            component:()=>import("./views/home/workflow/input/punish.vue"),
            meta:{
              label:"录入",
              urls:{
                list:"ratify/punishlist",
                select:["Users/getMyClass"]
              }
            }
          }
        ]
      },{
        path:"/workflow/input/family/add",
        component: () => import('./views/home/lifeServe/rating/model/add.vue'),
        meta:{
          label:"家庭困难添加",
          urls:{
            add:"ratify/familyadd",
            select:["WorkTime/getYears"]
          }
        }
      },{
        path:"/workflow/input/punish/add",
        component: () => import('./views/home/lifeServe/rating/model/add.vue'),
        meta:{
          label:"惩处情况添加",
          urls:{
            add:"ratify/addpunish",
            select:["WorkTime/getYears"]
          }
        }
      },{
        path:"/workflow/approval",
        redirect:"/workflow/approval/userinfo",
        component: () => import('./views/home/workflow/approval/approval.vue'),
        children:[
          {
            path:"/workflow/approval/userinfo",
            component: () => import('./views/home/workflow/approval/userInfo.vue'),
            meta:{
              label:"审批",
              urls:{
                list:"ratify/studentinfolist",
                select:["Users/getMyClass"]
              }
            }
          },{
            path:"/workflow/approval/award",
            component: () => import('./views/home/workflow/approval/award.vue'),
            meta:{
              label:"审批",
              urls:{
                list:"ratify/studentawardlist",
                select:["Users/getMyClass"]
              }
            }
          },{
            path:"/workflow/approval/experence",
            component: () => import('./views/home/workflow/approval/experence.vue'),
            meta:{
              label:"审批",
              urls:{
                list:"ratify/postassistantlist",
                select:["Users/getMyClass"]
              }
            }
          }
        ]
      },
      


      // 考勤管理
      {
        path:"/attendance/sign",
        redirect:"/attendance/sign/addSign",
        component: () => import('./views/home/attendance/sign/sign.vue'),
        children:[
          {
            path:"/attendance/sign/addSign",
            component: () => import('./views/home/lifeServe/rating/model/add.vue'),
            meta:{
              label:"发起签到",
              urls:{
                add:"Attendance/startSignin",
                select:["Attendance/getClassList","Attendance/classRoom"]
              }
            }
          }
        ]
      },{
        path:"/attendance/sign/signDetail/:id",
        component: () => import('./views/home/attendance/sign/signDetail.vue'),

      },{
        path:"/attendance/level",
        component: () => import('./views/home/attendance/leave/level.vue'),
        meta:{
          label:"请假审批",
          urls:{
            list:"attendance/leaveList"
          }
        }
      },{
        path:"/attendance/mysign",
        component: () => import('./views/home/attendance/mysign/mysign.vue'),
        meta:{
          label:"我的签到记录",
          urls:{
            list:"attendance/signinRecord"
          }
        }
      },{
        path:"/attendance/mysign/detail",
        component: () => import('./views/home/attendance/mysign/model/detail.vue'),
        meta:{
          label:"签到详情",
          urls:{
            list:"attendance/signinDetail"
          }
        }
      },{
        path:"/attendance/evaluate",
        component: () => import('./views/home/attendance/evaluate/evaluate.vue'),
        meta:{
          label:"考勤评定",
          urls:{
            list:"attendance/signinData",
            select:["users/getMyClass"]
          }
        }
      },{
        path:"/attendance/evaluate/detail",
        component: () => import('./views/home/attendance/evaluate/model/detail.vue'),
        meta:{
          label:"考勤评定",
          urls:{
            list:"attendance/studentDetail",
          }
        }
      },

      // 信息
      {
        path:"/message/receive/detail/:id",
        component:() => import('./views/message/model/detail.vue'),
        meta:{
          label:"接收消息详情",
          urls:{
            detail:"message/details"
          }
        }
      },{
        path:"/message/send/detail/:id",
        component:() => import('./views/message/model/detail.vue'),
        meta:{
          label:"发出消息详情",
          urls:{
            detail:"message/details"
          }
        }
      },{
        path:"/message/rs/people/detail/:id",
        component:() => import('./views/message/model/people.vue'),
        meta:{
          label:"接收详情",
          urls:{
            detail:"message/msgReceive"
          }
        }
      },{
        path:"/message/mycollect/list",
        component:() => import('./views/message/collect/collect.vue'),
        meta:{
          label:"收藏",
          urls:{
            list:"message/collect"
          }
        }
      },{
        path:"/message/mycollect/list/detail/:id",
        component:() => import('./views/message/model/detail.vue'),
        meta:{
          label:"发出消息详情",
          urls:{
            detail:"message/details"
          }
        }
      },{
        path:"/messagegroup/group",
        component:() => import('./views/message/group/group.vue'),
        children:[
          {
            path:"/messagegroup/group/mycreate",
            component:() => import('./views/message/group/mycreate.vue'),
            meta:{
              label:"我创建的",
              urls:{
                list:"message/myGroup"
              }
            }
          },{
            path:"/messagegroup/group/myjoin",
            component:() => import('./views/message/group/join.vue'),
            meta:{
              label:"我创建的",
              urls:{
                list:"message/intrantGroup"
              }
            }
          }
        ]
      },{
        path:"/messagegroup/group/mycreate/detail/:id",
        component:() => import('./views/message/group/model/detail.vue'),
        meta:{
          label:"详情",
          urls:{
            detail:"message/groupDetail"
          }
        }
      },{
        path:"/messagegroup/group/myjoin/detail/:id",
        component:() => import('./views/message/group/model/detail.vue'),
        meta:{
          label:"详情",
          urls:{
            detail:"message/groupDetail"
          }
        }
      },{
        path:"/messagegroup/group/mycreate/add",
        component:() => import('./views/message/group/model/add.vue'),
      },{
        path:"/messagegroup/group/myjoin/add",
        component:() => import('./views/message/group/model/add.vue'),
      },{
        path:"/message/1/add",
        component: () => import('./views/home/lifeServe/rating/model/add.vue'),
        meta:{
          label:"添加消息",
          urls:{
            add:"message/addMsg"
          }
        }
      },



    // 个人中心
    {
      path: '/center/maillist',
      component: () => import('./views/center/maillist/maillist.vue'),
      meta: {
        label:"个人中心",
        urls: {
          list: "users/address_list",
          select: ["users/departmentList"],//下拉url 数组
        }
      }
    }, {
      path: '/center/password',
      component: () => import('./views/center/password/password.vue'),
      meta: {
        label:"修改密码",
        urls: {
          edit: "Users/changePwd"
        }
      }
    }, {
      path: '/center/photo',
      component: () => import('./views/center/photo/photo.vue'),
      meta: {
        label:"修改照片",
        urls: {
          list: "User/photo"
        }
      }
    }
  ]
})
