import Vue from 'vue'
import {
  Button,
  Field,
  CellGroup,
  Tabbar,
  Toast,
  TabbarItem,
  NoticeBar,
  PullRefresh,
  Lazyload ,
  ImagePreview ,
  Popup ,
  Picker ,
  Dialog ,
  Uploader 
} from 'vant';

var plugsArray=[
  Button,
  Field,
  CellGroup,
  Tabbar,
  Toast,
  TabbarItem,
  NoticeBar,
  PullRefresh ,
  Lazyload ,
  Popup ,
  Picker ,
  Dialog,
  Uploader 
]

window.ImagePreview=ImagePreview
for(var i of plugsArray){
  Vue.use(i)
}
