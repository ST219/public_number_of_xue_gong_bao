import allUrl from './baseF.js'

export default {
  data() {
    return {
      listData: [],
      total:0,
      info:{},
      originalData:null,//原始数据
      selectData: {
        data1: []
      },
      detailData: {},

      resquest: {
        list: "",
        delete: "",
        add: "",
        edit: "",
        select: [],

        detail: "",
        deleteDetail: "",
        editDetail: "",
        detailSelect: [],//详情下拉
      },
      pageInfo: {
        page: 1,
      },
      moreInfo: {
        show: false,//是否显示  已经没有更多数据了 || 上拉加载
      }
    }
  },
  methods: {
    /**
     * @name 详情
     * @param {*} item 
     */
    routerDetail(item) {
      var path = _g.getRouterPath(this)
      var id = item.id||item.tid
      this.$router.push(path + '/detail/' + id)
    },

    /**
     * @name 新增
     */
    routerAdd(){
      var path = _g.getRouterPath(this)
      this.$router.push(path + '/add')
    },

    /**
     * @name 初始化
     */
    init() {
      // allUrl.forEach((val, key) => {
      //   if (_router.history.current.path.indexOf(key) > -1) {
      //     for (var key1 in this.resquest) {
      //       this.$set(this.resquest, key1, val[key1] || "")
      //     }
      //   }
      // })

      for(var key in _router.history.current.meta.urls||{}){
        this.$set(this.resquest, key, _router.history.current.meta.urls[key] || "")
      }
      
      var isDetail = _g.getRouterParams(this).id > 0
      if (!isDetail) {
        this.getSelectData(this.resquest.select)
        if (this.resquest.list) this.getListData()
      } else {
        this.getSelectData(this.resquest.detailSelect)
        if (this.resquest.detail) this.getDetail()
      }
    },
    
    /**
     * @name 获取列表
     */
    getListData() {
      let params = {
        page: this.pageInfo.page,
      }
      params = { ...params, ...this.searchInfo }
      _g.apiPost(this.resquest.list, params).then(res => {
        if (this.pageInfo.page == 1) {
          $('.more-info').slideUp()
          this.$set(this, "listData", res.data || res.list || res) //数组的位置不同
          if(res.total)this.total=res.total
          if(res.info)this.info=res.info
          this.originalData=res
        } else {
          this.$set(this, "listData", this.listData.concat(res.data || res.list || res)) //数组的位置不同
        }
        if ((res.data || res.list || res).length !=10) {
          this.moreInfo.show = true
        } else {
          this.moreInfo.show = false
        }
      })
    },

    /**
     * @name 获取详情
     */
    getDetail(url) {
      var url = url || this.resquest.detail
      let params = {
        id: _g.getRouterParams(this).id
      }
      _g.apiPost(url, params).then(res => {
        this.$set(this, "detailData", res.data || res)
      })
    },

    /**
     * @name 详情删除
     * @param {*} url 
     */
    delDetail(url) {
      this.$dialog.confirm({
        title: '提示',
        message: '是否确认删除？'
      }).then(() => {
        var url = url || this.resquest.deleteDetail
        let params = {
          id: _g.getRouterParams(this).id
        }
        _g.apiPost(url, params).then(res => {
          _g.toMessage(res)
          if (!res.error) this.$router.go(-1)
        })
      })
    },

    /**
     * @name 翻页
     */
    getMoreInfo(){
      if(!this.moreInfo.show){
        this.pageInfo.page++
        this.getListData()
      }
    },

    /**
     * @name 下拉数据
     */
    getSelectData(urlArray) {
      if (!urlArray) return
      urlArray.forEach((ele, index) => {
        _g.apiPost(ele,{},undefined,false).then(res => {
          this.$set(this.selectData, ['data' + (index + 1)], res)
        })
      })
    },
  },
  mounted() {
    this.init()
  },
}