// 列表
const allUrls=[
  [
    "/lifeServe/email",
    {
      list:"service/teachermail",
    }
  ],[
    "/lifeServe/subway",
    {
      list:"service/trainstation",
    }
  ],[
    "/lifeServe/schoolbus",
    {
      list:"service/schoolbus"
    }
  ],[
    "/lifeServe/calendar",
    {
      list:"service/calendar"
    }
  ],[
    "/lifeServe/phone",
    {
      list:"service/schooltel"
    }
  ],[
    "/lifeServe/opinions",
    {
      list:"opinion/query",
      detail:"opinion/reply",
      deleteDetail:"opinion/delOpinion",
      editDetail:"opinion/replyInsert"
    }
  ],[
    "/lifeServe/support",
    {
      list:"opinion/subsidize",
    }
  ],
  // 我的
  [
    "/center",
    {
      list:"users/info",
    }
  ],[
    "/center/maillist",
    {
      list:"users/address_list",
      select:["users/departmentList"],//下拉url 数组
    }
  ],[
    "/center/password",
    {
      edit:"Users/changePwd"
    }
  ],[
    "/center/photo",
    {
      list:"User/photo"
    }
  ]
]

export default new Map(allUrls)