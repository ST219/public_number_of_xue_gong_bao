import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 初始化
import './assets/css/reset.css'
import './assets/css/common.css'
import './assets/css/richtext.css'

import './assets/js/constant.js'
import './plugs/vants.js'
import './plugs/vants.css'

Vue.config.productionTip = false

router.beforeEach((to,from,next)=>{
  store.state.dias.studentSelect.show=false
  store.state.dias.anotherSelect.show=false
  store.state.dias.randomSign.show=false
  store.state.dias.acheet.show=false

  store.state.dias.datetime.dateShow=false
  store.state.dias.datetime.timeShow=false
  store.state.dias.datetime.datetimeShow=false
  next()
})

router.afterEach((to,from)=>{
  // 判断是否已经登录过了 sessention
  if(!sessionStorage.getItem('number')&&to.path!=='/'){
    router.replace('/login')
  }
  if(to.meta.label)document.title=to.meta.label
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
