import _g from './global.js'
import axios from 'axios'
import qs from 'qs'
import Vue from 'vue';
import store from '../../store'
import router from '../../router'

window._g=_g  //公共方法
window.$axios=axios
window.qs=qs
window._bus=new Vue()
window._store=store
window._router=router

window.isProduction=true//是否是本地
window.PATH=window.isProduction?"/api/":"/"

window.showLoading=true //是否显示loading

window.busConstants={
  flashFooter:"flashFooter",//返回时修改底部导航
}