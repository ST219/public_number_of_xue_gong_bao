const isProduction = process.env.NODE_ENV === 'production';
module.exports = {
  // 基本路径
  baseUrl: process.env.NODE_ENV === 'production' ? '/' : '/',
  // 输出文件目录
  outputDir: 'dist',
  assetsDir: 'admin',
  productionSourceMap: false,

  devServer: {
    host: '192.168.10.112',
    port: 8001,

    // 设置代理
    proxy: {
      "/api": {
        target: "https://xgjw.ambcon.cn/index.php/teacherweb/", // 域名
        // target: "",
        ws: false, // 是否启用websockets
        changOrigin: true, //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
        pathRewrite: {
          '/api': ''
        }
      }
    }
  },
  
  // 打包配置
  configureWebpack: config => {
    config.externals = {
      'vue': 'Vue',
      'vuex': 'Vuex',
      'vue-router': 'VueRouter',
      'axios': 'axios',
      'vant':'vant'
    }
  }
}